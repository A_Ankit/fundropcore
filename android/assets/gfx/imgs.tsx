<?xml version="1.0" encoding="UTF-8"?>
<tileset name="imgs" tilewidth="305" tileheight="295" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="45" height="64" source="game images/peg_on.png"/>
 </tile>
 <tile id="1">
  <image width="45" height="64" source="game images/peg_off.png"/>
 </tile>
 <tile id="2">
  <image width="305" height="295" source="game images/diamond_on.png"/>
 </tile>
 <tile id="3">
  <image width="305" height="295" source="game images/diamond_off.png"/>
 </tile>
</tileset>
