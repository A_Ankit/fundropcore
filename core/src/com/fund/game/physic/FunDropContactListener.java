package com.fund.game.physic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

/**
 * Created by Mukesh on 2/12/2018.
 * Class for handling contacts
 */

public class FunDropContactListener implements ContactListener{
  public static final String TAG = FunDropContactListener.class.getSimpleName();
  @Override
  public void beginContact(Contact contact) {
    Fixture fixtureA = contact.getFixtureA();
    Fixture fixtureB = contact.getFixtureB();
    Gdx.app.log(TAG,"FixtureA begin Contact:"+fixtureA.toString());
    Gdx.app.log(TAG,"FixtureB begin Contact:"+fixtureB.toString());
    //TODO : desired action for these fixtures
  }

  @Override
  public void endContact(Contact contact) {
    Fixture fixtureA = contact.getFixtureA();
    Fixture fixtureB = contact.getFixtureB();
    Gdx.app.log(TAG,"FixtureA end Contact:"+fixtureA.toString());
    Gdx.app.log(TAG,"FixtureB end Contact:"+fixtureB.toString());
  }

  @Override
  public void preSolve(Contact contact, Manifold oldManifold) {

  }

  @Override
  public void postSolve(Contact contact, ContactImpulse impulse) {

  }
}
