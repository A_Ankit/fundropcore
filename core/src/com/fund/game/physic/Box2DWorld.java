package com.fund.game.physic;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.fund.game.screen.GameScreen;

/**
 * Created by prokarma on 2/13/2018.
 */

public class Box2DWorld {
  public static final int COLLISION_LAYER = 3;
  private World world;
  private TiledMap tiledMap;

  private Shape shape;
  private BodyDef bdef;
  private FixtureDef fdef;
  private Body body;
  // private AbstractScreen screen;
  private GameScreen screen;

  public Box2DWorld(GameScreen screen) {
    this.screen = screen;
    tiledMap = screen.getTiledMap();
    world = screen.getWorld();
  }

  public void createWorld(){
    for (MapObject object : tiledMap.getLayers().get(COLLISION_LAYER).getObjects().getByType(PolylineMapObject.class)) {
      shape = getPolyline((PolylineMapObject) object);
      createPhysicBody(shape);
    }

    for (MapObject object : tiledMap.getLayers().get(COLLISION_LAYER).getObjects().getByType(RectangleMapObject.class)) {
      shape = getRectangle((RectangleMapObject) object);
      createPhysicBody(shape);
    }
  }

  public void createPhysicBody(Shape shape){
    bdef = new BodyDef();
    fdef = new FixtureDef();
    bdef.type = BodyDef.BodyType.StaticBody;
    body = world.createBody(bdef);
    fdef.shape = shape;
    fdef.friction = 1.0f;
    body.createFixture(shape, 0);
  }

  //Create shape and set its vertices
  private ChainShape getPolyline(PolylineMapObject polylineObject) {
    float[] vertices = polylineObject.getPolyline().getTransformedVertices();
    Vector2[] worldVertices = new Vector2[vertices.length / 2];

    for (int i = 0; i < vertices.length / 2; ++i) {
      worldVertices[i] = new Vector2();
      worldVertices[i].x = vertices[i * 2];
      worldVertices[i].y = vertices[i * 2 + 1];
    }

    ChainShape chain = new ChainShape();
    chain.createChain(worldVertices);
    return chain;
  }

  private PolygonShape getRectangle(RectangleMapObject rectangleObject) {
    Rectangle rectangle = rectangleObject.getRectangle();
    PolygonShape polygon = new PolygonShape();
    Vector2 size = new Vector2((rectangle.x + rectangle.width * 0.5f),
        (rectangle.y + rectangle.height * 0.5f));
    polygon.setAsBox(rectangle.width * 0.5f,
        rectangle.height * 0.5f,
        size,
        0.0f);
    return polygon;
  }

}
