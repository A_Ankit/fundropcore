package com.fund.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fund.game.manager.ScreenManager;
import com.fund.game.screen.ScreenType;

public class FunDropGame extends Game {
	private static FunDropGame funDropGame =  new FunDropGame();

	SpriteBatch batch;
	AssetManager assetManager;

	private FunDropGame(){

	}

	public static FunDropGame getInstance(){
		return funDropGame;
	}

	@Override
	public void create () {
		batch = new SpriteBatch();
		assetManager = new AssetManager();
		ScreenManager.getInstance().setScreen(batch, assetManager, ScreenType.GAME_SCREEN, null);
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
		batch.dispose();
		assetManager.dispose();
	}
}
