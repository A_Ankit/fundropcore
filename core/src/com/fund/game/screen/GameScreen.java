package com.fund.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.fund.game.manager.resource.AbstractAssetManager;
import com.fund.game.physic.Box2DWorld;
import com.fund.game.physic.FunDropContactListener;
import com.fund.game.sprite.Pinball;
import com.fund.game.util.Constant;
import java.util.concurrent.atomic.AtomicInteger;

public class GameScreen extends AbstractScreen{
  public static final String TAG = GameScreen.class.getSimpleName();
  private TiledMap tiledMap;
  private OrthogonalTiledMapRenderer renderer;

  private World world;
  Box2DWorld box2DWorld;
  private Box2DDebugRenderer box2DDebugRenderer;

  private Pinball pinball;

  SpriteBatch batch;
  private boolean isPressing;
  private AtomicInteger pressCount = new AtomicInteger(0);

  public GameScreen(SpriteBatch batch,
      AbstractAssetManager assetManager, ScreenType screenType) {
    super(batch, assetManager, screenType);

    this.batch = new SpriteBatch();

    tiledMap = assetManager.getAssetManager().get(Constant.GAME_MAP);
    renderer = new OrthogonalTiledMapRenderer(tiledMap);

    world = new World(new Vector2(0, -5f), true);

    box2DWorld = new Box2DWorld(this);
    box2DWorld.createWorld();

    world.setContactListener(new FunDropContactListener());

    box2DDebugRenderer = new Box2DDebugRenderer();
    pinball = new Pinball(this);
    registerMultiplexer();
  }

  @Override
  public void render(float delta) {
    super.render(delta);
    //timeStep is frequency.
    //Velocity/position iteration to correct collision reaction
    world.step(1/60f, 7, 2);

    pinball.update(delta);

    camera.update();
    renderer.setView(camera);
    renderer.render();

    batch.begin();
    Gdx.app.log(TAG, "pinball x:"+pinball.getX());
    Gdx.app.log(TAG, "pinball y:"+pinball.getY());
    batch.draw(pinball.getTexture(), pinball.getX(), pinball.getY());
    batch.end();

    box2DDebugRenderer.render(world, camera.combined);
    handleInput(delta);
  }

  @Override
  public void drawSpriteBatch() {
    pinball.draw(batch);
  }

  @Override
  public void dispose() {
    tiledMap.dispose();
    renderer.dispose();
    world.dispose();
    super.dispose();
  }

  public TiledMap getTiledMap() {
    return tiledMap;
  }

  public World getWorld() {
    return world;
  }



  /**
   * Function to handle Input
   * TODO: Change as per need
   * @param dt
   */
  private void handleInput(float dt){

    if (isPressing){
      pressCount.incrementAndGet();
    }else {
      if (pressCount.get()>0){
        pinball.releaseBall((float)pressCount.get());
        pressCount.set(0);
      }
    }
  }


  private void registerMultiplexer(){
    InputMultiplexer multiplexer = new InputMultiplexer();
    multiplexer.addProcessor(new InputProcessor() {
      @Override
      public boolean keyDown(int keycode) {
        return false;
      }

      @Override
      public boolean keyUp(int keycode) {
        return false;
      }

      @Override
      public boolean keyTyped(char character) {
        return false;
      }

      @Override
      public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        Gdx.app.log(TAG, "touch down:"+pointer);
        isPressing = true;
        return false;
      }

      @Override
      public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        isPressing = false;
        return false;
      }

      @Override
      public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
      }

      @Override
      public boolean mouseMoved(int screenX, int screenY) {
        return false;
      }

      @Override
      public boolean scrolled(int amount) {
        return false;
      }
    });
    multiplexer.addProcessor(stage);
    Gdx.input.setInputProcessor(multiplexer);

  }
}
