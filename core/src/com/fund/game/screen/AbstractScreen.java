package com.fund.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.fund.game.manager.resource.AbstractAssetManager;

public abstract class AbstractScreen implements Screen{
  public static final int WORLD_WIDTH = 1440;
  public static final int WORLD_HEIGHT = 2560;
  public Viewport viewport;
  protected Stage stage;
  //public SpriteBatch batch;

  OrthographicCamera camera;

  private ScreenType screenType;
  private AbstractAssetManager assetManager;

  public AbstractScreen(SpriteBatch batch, AbstractAssetManager assetManager, ScreenType screenType) {
    this.screenType = screenType;
    //this.batch = batch;

    camera = new OrthographicCamera();
    viewport = new FillViewport(WORLD_WIDTH, WORLD_HEIGHT, camera);
    camera.zoom = 1f;
    camera.position.set(viewport.getWorldWidth(), viewport.getWorldHeight(), 0f);

    stage = new Stage(viewport, batch);
    Gdx.input.setInputProcessor(stage);

    this.assetManager = assetManager;
    this.assetManager.loadAsset();
    assetManager.getAssetManager().finishLoading();
  }

  public ScreenType getScreenType() {
    return screenType;
  }

  public void setScreenType(ScreenType screenType) {
    this.screenType = screenType;
  }

  @Override
  public void show() {

  }

  @Override
  public void render(float delta) {
    Gdx.gl.glClearColor(1, 0, 0, 1);
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    //batch.setProjectionMatrix(viewport.getCamera().combined);
    //SpriteBatch method must called between begin() and end()
    /*batch.begin();
    drawSpriteBatch();
    batch.end();*/

    stage.act();
    stage.draw();
  }

  @Override
  public void resize(int width, int height) {
    viewport.update(width, height);
  }

  @Override
  public void pause() {

  }

  @Override
  public void resume() {

  }

  @Override
  public void hide() {

  }

  @Override
  public void dispose() {
    stage.dispose();
    assetManager.unloadAsset();
  }

  public abstract void drawSpriteBatch();
}
