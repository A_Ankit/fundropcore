package com.fund.game.manager.resource;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.fund.game.util.Constant;

/**
 * Created by Mukesh on 2/8/2018.
 * Manages Game Assets
 */

public class GameAssetManager extends AbstractAssetManager{


  public GameAssetManager(AssetManager assetManager) {
    super(assetManager);

  }

  @Override
  void loadGraphics() {
    getAssetManager().setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
    getAssetManager().load(Constant.GAME_MAP, TiledMap.class);
    //getAssetManager().load(Constant.GAME_TILE, TextureAtlas.class);
  }

  @Override
  void loadSounds() {

  }

  @Override
  void loadFonts() {

  }

  @Override
  void unloadGraphics() {
    getAssetManager().unload(Constant.GAME_TILE);
  }

  @Override
  void unloadSounds() {

  }

  @Override
  void unloadFonts() {

  }
}
