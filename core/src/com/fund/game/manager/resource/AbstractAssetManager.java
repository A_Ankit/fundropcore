package com.fund.game.manager.resource;

import com.badlogic.gdx.assets.AssetManager;

/**
 * Created by Mukesh on 2/8/2018.
 * Manager class for handling assets
 */

public abstract class AbstractAssetManager {

  private AssetManager assetManager;

  public AbstractAssetManager(AssetManager assetManager) {
    this.assetManager = assetManager;
  }

  public void loadAsset(){
    loadGraphics();
    loadSounds();
    loadFonts();
  }

  public void unloadAsset(){
    unloadGraphics();
    unloadSounds();
    unloadFonts();
  }

  abstract void loadGraphics();

  abstract void loadSounds();

  abstract void loadFonts();

  abstract void unloadGraphics();

  abstract void unloadSounds();

  abstract void unloadFonts();

  public AssetManager getAssetManager(){
    return assetManager;
  }
}
