package com.fund.game.manager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fund.game.FunDropGame;
import com.fund.game.manager.resource.AbstractAssetManager;
import com.fund.game.manager.resource.GameAssetManager;
import com.fund.game.screen.AbstractScreen;
import com.fund.game.screen.GameScreen;
import com.fund.game.screen.ScreenType;

/**
 * Created by Mukesh on 2/8/2018.
 * Manager for switching Screen
 */

public class ScreenManager {
  private static ScreenManager screenManager;

  AbstractScreen currentScreen;

  private ScreenManager() {
  }

  public static ScreenManager getInstance() {
    if (screenManager==null){
      screenManager = new ScreenManager();
    }
    return screenManager;
  }

  public void setScreen(SpriteBatch batch, AssetManager assetManager, ScreenType screenType, AbstractScreen oldScreen){
    switch (screenType){
      case GAME_SCREEN:
        currentScreen = new GameScreen(batch, new GameAssetManager(assetManager), screenType);
        break;
    }
    FunDropGame.getInstance().setScreen(currentScreen);
    if (oldScreen!=null){
      oldScreen.dispose();
    }
  }

  public void getScreen(){

  }

  /*public void setCurrentScreenType(ScreenType screenType){

  }*/
}
