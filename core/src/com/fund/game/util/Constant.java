package com.fund.game.util;

public class Constant {
  //Asset Constant
  public static final String GRAPHIC_DIR = "gfx/";
  public static final String FONT_DIR = "font/";
  public static final String SOUND_DIR = "audio/";

  //GAME SCREEN
  public static final String GAME_TILE= GRAPHIC_DIR+"imgs.tsx";
  public static final String GAME_MAP = GRAPHIC_DIR+"fundroptable.tmx";
  public static final String PEG_ON = GRAPHIC_DIR+"peg_on.png";

}
