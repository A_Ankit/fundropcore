package com.fund.game.sprite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.fund.game.screen.GameScreen;
import com.fund.game.util.Constant;

/**
 * Created by prokarma on 2/13/2018.
 */

public class Pinball extends Sprite{
  public static final String TAG = Pinball.class.getSimpleName();
  public World world;
  public Body b2body;

  private Texture pinballTexture;

  private GameScreen gameScreen;

  float radius = 30f;

  public Pinball(GameScreen gameScreen) {
    this.gameScreen = gameScreen;
    world = gameScreen.getWorld();
    pinballTexture = new Texture(Gdx.files.internal(Constant.PEG_ON));
    b2body = initBallBody();
    setBounds(GameScreen.WORLD_WIDTH/2,GameScreen.WORLD_HEIGHT/2, 1/100, 1/100);
    setTexture(pinballTexture);
  }

  private Body initBallBody(){

    CircleShape sd = new CircleShape();
    sd.setRadius(radius);

    FixtureDef fdef = new FixtureDef();
    fdef.shape = sd;
    fdef.density = 1.0f;
    fdef.friction = 0.3f;
    fdef.restitution = 0.6f;

    BodyDef bd = new BodyDef();
    bd.allowSleep = true;
    bd.position.set(GameScreen.WORLD_WIDTH - 64,GameScreen.WORLD_HEIGHT/2);
    Body ballBody = world.createBody(bd);
    ballBody.createFixture(fdef);
    ballBody.setType(BodyType.DynamicBody);
    ballBody.setBullet(true);
    ballBody.getFixtureList().get(0).setDensity((0.5f*0.5f) / (radius*radius));
    ballBody.resetMassData();
    return ballBody;
  }

  /**
   * Update ball texture position
   * @param dt delta time
   */
  public void update(float dt){
    setPosition(b2body.getPosition().x/2 - getWidth()-radius, b2body.getPosition().y/2 - getHeight()-radius);
  }

  public void draw(SpriteBatch batch){
    batch.draw(pinballTexture, getX(), getY());
    Gdx.app.log(TAG, "pinball text1:"+getX());
    Gdx.app.log(TAG, "pinball text1:"+getY());
    if (pinballTexture!=null){
      super.draw(batch);
    }
  }

  public void releaseBall(float velocity){
    Gdx.app.log(TAG, "Release velocity:"+velocity*1000);
    b2body.applyLinearImpulse(new Vector2(0f, velocity*1000), b2body.getWorldCenter(), true);
  }
}
